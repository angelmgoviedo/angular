import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './../models/destinos-viajes-state.model';
import {Injectable, Inject, forwardRef} from '@angular/core';
import { HttpRequest, HttpHeaders, HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
	destinos: DestinoViaje[] = [];

  constructor(
	private store : Store<AppState>,
	@Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
	private http: HttpClient) {
		this.store.select(state => state.destinos)
		.subscribe((data) => {
			this.destinos= data.items;
		});
	}
	add(d:DestinoViaje){
		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
		const req = new HttpRequest('POST', this.config.apiEndPoint + '/my', {nuevo: d.nombre}, {headers: headers });
		this.http.request(req).subscribe((data: HttpResponse<{}>) => {
			if(data.status === 200){
				this.store.dispatch(new NuevoDestinoAction(d));
				const myDb = db;
				myDb.destinos.add(d);
				myDb.destinos.toArray().then(destinos => console.log(destinos))
			}
		});
	}

	elegir(d: DestinoViaje) {
		this.store.dispatch(new ElegidoFavoritoAction(d));	
	}
	getById(id: string): DestinoViaje {
		console.log('principal');
		return null;
	}
	getAll(): DestinoViaje[] {
		return this.destinos;
	}

} 