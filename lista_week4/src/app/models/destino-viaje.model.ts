export class DestinoViaje{
	nombre: string;
	imagenUrl: string;
	public votes: number;
	private selected: boolean;
	public servicios: string[];
	constructor(nombre:string, imagenUrl:string){ 
		this.nombre=nombre;
		this.imagenUrl=imagenUrl;
		this.servicios = ['desayuno','comida'];
		this.votes=0;
	}
	isSelected(): boolean {
		return this.selected;
	}
	setSelected(s: boolean) {
		this.selected=s;
	}
	voteUp() {
		this.votes++;
	}
	voteDown() {
		this.votes--;
	}
}