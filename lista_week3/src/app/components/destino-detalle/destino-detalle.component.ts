import { Component, OnInit } from '@angular/core';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { DestinoViaje } from './../../models/destino-viaje.model';


@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [
	DestinosApiClient
  ]
})
export class DestinoDetalleComponent implements OnInit {

  constructor( private destinosApiClient: DestinosApiClient) { }

  ngOnInit() {
  }

}
